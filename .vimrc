""" KEY BINDINGS

" Insert new line without entering insert-mode by pressing enter or shift-Enter
map <Enter> o<ESC>
map <S-Enter> O<ESC>

""" LAYOUT

" Show line numbers by default
set number

" Enable syntax highlighting
syntax on

" Set color scheme
colorscheme desert

""" THEMING