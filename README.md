# Dotfiles
My personal dotfiles for __tmux__ and __vim__.

## Installation
1. Place ``.tmux.conf`` and ``.vimrc`` in your home folder
2. Reload tmux and vim config as stated below

## Reload tmux config
```bash
:source-file ~/.tmux.conf
```

## Reload vim config
```bash
:source ~/.vimrc
```

## Reload zsh config
```bash
source ~/.zshrc
```